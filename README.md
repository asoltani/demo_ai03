# Demo AI03

Instructions :

* Ajouter les codes sources de GoogleTest à la racine du projet : demo_AI03/googletest

* Compiler le projet avec CMake en utilisant les fonctionnalités d'un IDE (CLion a été utilisé pour la démo) 
ou à la main avec les commande suivantes:  
- mkdir build  
- cd build  
- cmake ..  
- make gtest_demo_AI03  

* enfin exécuter les tests avec la commande (ou en utilisant un IDE tel que CLion):
- ./tests/gtest_demo_AI03    


PS: CLion implémente également un post traitement des résultats afin de les présenter plus graphiquement
voir : https://www.jetbrains.com/help/clion/unit-testing-tutorial.html
