#include <gtest/gtest.h>
#include <stdexcept>

#include "../library.h"


TEST(DummyTests, test_exemple1)
{
//    EXPECT_TRUE(true);
//    EXPECT_FALSE(false);
//    EXPECT_EQ(5, 42);
//
//    ASSERT_TRUE(false);
//    EXPECT_FALSE(true);
}

TEST(DummyTests, test_exemple2)
{
    EXPECT_THROW(algo_super_complexe(0), std::invalid_argument);

    ASSERT_EQ(algo_super_complexe(5), 1.0f / 5.0f);

    for (int i = 0; i < 7; ++i) {
//        EXPECT_TRUE(g());
         EXPECT_TRUE(g()) << "i = " << i;
    }
}