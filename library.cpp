#include "library.h"

#include <iostream>

float algo_super_complexe(int a) {
    if (a == 0)
        throw std::invalid_argument("a doit être différent de 0");

    return 1.0f / static_cast<float>(a);
}

bool g() {
    static int count_g = 0;
    return (count_g++) < 5;
}
